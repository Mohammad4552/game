package co.simplon.promo18;

import java.util.Scanner;

public class TicTocToe {

  public void Game() {
    Scanner in = new Scanner(System.in);
    System.out.println("player1, comment tu t'appelle");
    String p1 = in.nextLine();
    System.out.println("player2, comment tu t'appelle");
    String p2 = in.nextLine();

    Board board = new Board();

    board.writeSymbolOnALLBoard();

    boolean isPlayer1 = true;

    boolean jeuTerminé = false;
    

    while (!jeuTerminé) 
    {

      board.drawBoard();

      String symbol = " ";
      if (isPlayer1) {
        symbol = "[x]";
      } else {
        symbol = "[o]";
      }

      if (isPlayer1) {
        System.out.println(p1 + " tour (x);");
      } else {
        System.out.println(p2 + " tour (o);");
      }

      int row = 1;
      int col = 1;

      while (true) {

        System.out.println("Entrer une row, (1, 2 ou 3)");
        row = in.nextInt();
        System.out.println("Entrer un col, (1, 2 ou 3)");
        col = in.nextInt();

        if (row < 1 || col < 1 || row > 3 || col > 3) {
          System.out.println("row et col hors limites");
        } else if (board.board1[row][col] != "[ ]") {
          System.out.println("quelqu'un a déjà fait un geste");

        } else {

          break;
        }

      }

      board.board1[row][col] = symbol;

      if (aGagné(board.board1) == "[x]") {

        System.out.println(p1 + " a gagné");
        jeuTerminé = true;

      } else if (aGagné(board.board1) == "[o]") {
        System.out.println(p2 + " a gagné");
        jeuTerminé = true;

      } else {
        if (aCravate(board.board1)) {
          System.out.println("a cravate");
          jeuTerminé = true;

        } else {
          isPlayer1 = !isPlayer1;
        }
      }
      if (jeuTerminé) {
        
      
      System.out.println("Voulez-vous rejouer? (1 = oui/ 2 = non)");
      int jouer = in.nextInt();
      if (jouer == 2) {
        jeuTerminé = true;
      } else {
        jeuTerminé = false;
        board.writeSymbolOnALLBoard();
    
       }
      }

    }

    board.drawBoard();

  

  }

  public static String aGagné(String[][] board) {
    for (int i = 1; i < 4; i++) {
      if (board[i][1] == board[i][2] && board[i][2] == board[i][3] && board[i][1] != "[ ]") {
        return board[i][1];

      }
    }

    // col
    for (int j = 1; j < 4; j++) {
      if (board[1][j] == board[2][j] && board[2][j] == board[3][j] && board[1][j] != "[ ]") {
        return board[1][j];

      }

    }

    if (board[1][1] == board[2][2] && board[2][2] == board[3][3] && board[1][1] != "[ ]") {
      return board[1][1];
    }

    if (board[3][1] == board[2][2] && board[2][2] == board[1][3] && board[3][1] != "[ ]") {
      return board[3][1];
    }

    return "[ ]";

  }

  public static boolean aCravate(String[][] board) {
    for (int i = 1; i < 4; i++) {
      for (int j = 1; j < 4; j++) {
        if (board[i][j] == "[ ]") {
          return false;
        }

      }

    }
    return true;

  }

}
